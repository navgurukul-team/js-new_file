const list = new Promise ((resolve , reject)=>{
    let flag = false
    if(!flag){
        setTimeout(()=>{
            console.log("This is a name list...");
            resolve(["shivam" , "sonu ", "satyam","suraj"])

        }, 1000)
    }else{
        setTimeout(()=>{
            reject("error : something is wrong")
        } , 1000)
    }   
})

// When you use asycn or await without useing try or catch so that time he didn't handel your error.
const nameList = async () =>{
    let response = await list
    console.log(response);
}

nameList()


const user = new Promise ((resolve , reject)=>{
    let flag = false
    if(!flag){
        setTimeout(()=>{
            console.log("User id Loaded wait a min...");
            resolve({username: "shivam kumar", password: 123})

        }, 1000)
    }else{
        setTimeout(()=>{
            reject("error : something is wrong")
        } , 1000)
    }   
})

const userdata = async() =>{
    try {
        let response = await user
        console.log(response);
    } catch (error) {
        console.log(error);
    }
}
userdata()



const allData = async ()=>{
    try {
        const response = await fetch("https://jsonplaceholder.typicode.com/posts")
        const data = await response.json()   // Data take time convert in object so you always use await when conver data in json
        console.log(data);
    } catch (error) {
       console.log(error); 
    }
    
}
allData()


// list.then((vlaue)=>{
//     console.log("showing your list ",vlaue);
// }).catch((value2)=>{
//     console.log("something is worng " , value2);
// })