function data (callback){
    console.log("first data");
    callback()
}

function allData(){
    console.log("All type data here ...");
}

data(allData)


let stock = {
    fruits :["strawberry" , "grapes" , "mango" , "banana" , "apple"],
    liquid : ["water", "ice"],
    holder :["cone" , "cup" , "stick"],
    toppings: ["chocolate" , "peanuts"]
}

console.log(stock.fruits[1], stock.liquid[0], stock.holder[1], stock.toppings[0]);

const Order = (fruitsName, liquidName , holderName , topping , myCallBack)=>{

    setTimeout(()=>{
        console.log(`${stock.fruits [fruitsName]} + ${stock.liquid [liquidName]} + ${stock.holder [holderName]} + ${stock.toppings [topping]} = We have received your order`);

        myCallBack()

    } , 3000)

}
const production = () =>{

    setTimeout(()=>{
        console.log("production has started now wait for some time...");
    } , 2000)
}

Order(2 , 1 , 0 , 1 , production)



const iceCreem = (time , work) => {

    let flag = true
    return new Promise ((resolve , reject) => {

        if(flag){
            setTimeout(()=>{
                resolve (work ())

            } , time)
        }
        else{
            reject ( )
        }
    })
}

iceCreem (1000 , () => console.log(`${stock.fruits [2]} fruits has selected`))

    .then(()=>{
        return iceCreem(2000 , ()=>{
            console.log(`${stock.liquid [1]} has been selected`);
            console.log("process has start...");
        })
    })
    .then(()=>{
        return iceCreem(1000 , () =>{
            console.log(`${stock.holder[1]} has selected`);

        })
    })
    .then(()=>{
        return iceCreem(1000 , ()=>{
            console.log(`${stock.toppings[1]} has been selected`);
        })
    })
    .then(()=>{
        return iceCreem(1000 , ()=>{
            console.log( "Your ice creem is ready...");
        })
    })
    .catch(()=>{
        console.log("sorry sir The shop has closed \nplease come next day...");
    })
    .finally (()=>{
        console.log("Thank you for coming sir/mam");
    })