// const user_id = [
//     {
//         Name: 'shivam ',
//         country: 'india',
//         state: 'Bihar',
//         city: 'jamui',
//         Age: 22,
//     },
//     {
//         Name: 'Ayus sharma',
//         country: 'india',
//         state: 'ujjian',
//         Age: 25,

//     }
// ]
// console.log(user_id)

// if ('Name' in user_id && 'Age' in user_id){
//     console.log(`Name: ${user_id.Name}, Age: ${user_id.Age}`);
// }else{
//     console.log("not")
// }


const person = {
    first_name: "shivam",
    last_name: "kumar",
    age : 20,
    clothes : ["jeans" , "shirt" , "shorts" , "lower"],  // object under array
    living : {
        "city" : "jamui",
        "country" : "india",                             // object under object

    }

}
// console.log(person)  //how to print object 
// console.log(person.first_name) // how to print object each value

// person.num = 9304352368     // how to add in object
// console.log(person)    

// person.first_name = "rishav"  // modify your object 
// console.log(person) 

// delete person.last_name    // delete element in object
// console.log(person)

// console.log("age" in person)  // check your propeties exist or not

// for(let key in person){
//     console.log(key)          // how to print your all keys
// }

// for (let key in person){
//     console.log(key + ":"+ person[key]) // how to print all key and value in object
// }

// console.log(person.clothes[2])   // print perticular index value

// console.log(person.living)         // print object under living object 

// console.log(person.living.country)  // print living object in country value


// let clothes = {
//     jeans_brand : "killer_jeans",
//     shirt_brand : "levi's_shirt",
//     shorts : "ck_shorts",

//     all_clothes : function(){
//         return this.jeans_brand + " , " + this.shirt_brand
//     }
// }
// console.log(clothes.all_clothes( ))