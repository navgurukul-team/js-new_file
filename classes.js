class fullName {
    constructor(sName,lName){
        this.sName = sName
        this.lName = lName
    }
    sName(){
        console.log('shivam');
    }
}

let obj1 = new fullName('shivam','kumar');
console.log(obj1);


class Animal {
    constructor(name, sound) {
        this.name = name;
        this.sound = sound;
    }

    makeSound() {
        console.log(`${this.name} says ${this.sound}!`);
    }
}

// Creating instances of the Animal class
const dog = new Animal('Dog', 'Woof');
const cat = new Animal('Cat', 'Meow');

// Calling the makeSound method on the instances
dog.makeSound(); // Output: Dog says Woof!
cat.makeSound(); // Output: Cat says Meow!


class books{
    constructor(book_name , history){
        this.book_name = book_name
        this.history = history
    }
}
const all_books = new books("Alla din","20/08/23")
console.log(all_books)


class Id {
    constructor(Name,DOB,Address,Mob){
        this.Name = Name
        this.DOB = DOB
        this.Address = Address
        this.Mob = Mob
    }
}
const user1 = new Id("shivam","25/09/2000","jamui","9304352368") 
const user2 = new Id("Ayush","05/05/198","ujjain","9304352368") 
console.log(user1,user2)


// default properties
class user{
    constructor(
        name = "shivam kumar",
        age = 20,
        country = "india",
        state = "Bihar",

    )
    {
        this.name = name
        this.age = age
        this.country = country
        this.state = state
    }
}
const id = new user()
const id2 = new user("Ayush",25,"india","ujjain")

console.log(id)
console.log(id2)



class store{
    constructor(milk_cake,laddu,sweet){
        this.milk_cake = milk_cake
        this.laddu = laddu
        this.sweet = sweet

    }
    all_in_one() {
       const all_in_one = this.milk_cake + " " + this.laddu + " " + this.sweet
       return all_in_one

    }
    get getladdu(){
        return this.laddu
    }

}
let store1 = new store("cream_cake","motti_chur","chumchum")
let store2 = new store("non_cream","peele_laddu","gulab_jamun")

console.log(store1.all_in_one())
console.log(store2.all_in_one())
console.log(store.laddu)





class Tech {
    constructor(vivo , oppo , samsung , redmi){
        this.vivo = vivo
        this.oppo = oppo
        this.samsung = samsung
        this.redmi = redmi
    }  
    vivo_model(){
        return vivo

    }
    vivo_model(){
        return oppo

    }
    samsung_model(){
        return samsung
        
    }
    redmi_model(){
        return redmi

    }


}
let vivo = {
    vivo21: "vivo_21",
    vivof15: "vivo_f15",
    vivov17: "vivo_v17_pro",
};

let oppo = {
    oppo_set_01 : "Oppo_Reno",
    oppo_set_02 : "Oppo_A15",
    oppo_set_03 : "Oppo_Reno_2",
    oppo_set_04 : "Oppo_A20_pro",
}

let samsung = {
    samsung_set_01: "samsung_A2",
    samsung_set_02: "samsung_S20",
    samsung_set_03: "samsung_S21",
    samsung_set_04: "samsung_S22",
}

let redmi = {
    redmi_set_01 : "redmi_5A",
    redmi_set_02 : "redmi_6A",
    redmi_set_03 : "redmi_k20",
    redmi_set_04 : "redmi_10",
}

let New_model = new Tech("vivo_T1" , "Oppo_A15","Samsung_S21","redmi_k20")  // object 
console.log(New_model)


let model = new Tech (vivo ,oppo , samsung ,redmi);
console.log(vivo);


