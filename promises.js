// A promises is a promises of code execution . The code either executes or fail in both the case the subscriber will be notified 

// This is Asynchronous function 
promise =()=>{
    setTimeout(()=>{
        console.log("hello shivam ")
    }, 1000)
    console.log("I am your server")
    console.log("hey")   
}
promise()



// how to create function without name. 
new Promise((resolve , reject)=>{
    console.log("Please wait your program is loading...");
    setTimeout(()=>{
        console.log("Your name is access...");
        resolve()
        
    } , 1000)
    
}).then(()=>{
    console.log("program is finished");
}).catch(()=>{
    console.log("not found");
})


// How to use object in promise
new Promise((resolve , reject)=>{
    console.log("Please wait your program is loading...");
    setTimeout(()=>{
        console.log("Your name is access...");
        resolve({Name: "shivam", Add: "Jamui", profession: "Student" })
        
    } , 1000)
    
}).then((user)=>{
    console.log("program is finished..." ,user);
}).catch(()=>{
    console.log("not found");
})





// how to use callback hell basiclly when you use then under then function it called callback hell
new Promise((resolve , reject)=>{
    console.log("Please wait your program is loading...");
    setTimeout(()=>{
        let flag = true
        if(!flag)(
            resolve({Name: "shivam", Add: "Jamui", profession: "Student" })

        )
        else{
            reject("Error : something went wrong")
        }
        
    } , 1000)
    
}).then((user)=>{
    console.log(user);
    return user.newUser
}).then((newUser)=>{
    console.log(newUser);
}).catch((error)=>{
    console.log("not found");
})




const book = new Promise((resolve , reject ) =>{
    console.log("promise is pending ")
    setTimeout(() => {
        let flag = false;
        if(flag){
            // console.log("I am a promise and I am fulfilled")
            resolve(true)
        }else{
            // console.log("error")
            reject(false)
        }
        
    }, 1000);
   
})

// // console.log(book)  // when check your program is pending or not simply you write your argument in log and then print 

// //when you work in both the case in one time so that time he excute perlel

// //In promise using .than or .catch method we are notified value will be or error will be get .

// and every promise in one state or one resulte he they work internelly .
book.then((dataTrue)=>{
    console.log("I am solved ");
})
.catch((dataFlase)=>{
    console.log("I am not solved")
})




// Async v/s Await :=> Anyone function we can convert in async function.
// And after create async function We can make promises and await in this function. and async function always returen promises.

// promise returen said like i give you a when i get a data i will returne you.


// How to use promise in async 
async function weather() {
    let himachelWeather = new Promise((resolve , reject) =>{
        setTimeout(()=>{
            resolve("15 Deg")
        } , 1000)
    })
    let jamuiWeather = new Promise((resolve , reject) => {
        setTimeout(()=>{
            reject("16 Deg")
        } , 5000)
    })
    himachelWeather.then(alert)
    jamuiWeather.catch(alert)
   
}
weather()


const shivam = '{"key":"value","number":42,"bool":true}';
const myObject = JSON.parse(shivam);
console.log(myObject);

const MyObject = { key: 'value', number: 42, bool: true };
const jsonString = JSON.stringify(MyObject);
console.log(jsonString);
