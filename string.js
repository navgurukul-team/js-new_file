// \n use for new line
// console.log("A JavaScript string stores \n a series of characters like John Doe");


// // \t use for tab he take 8 space
console.log("A JavaScript \tstring stores")

// // back \\ uses
console.log("A JavaScript \\string stores")

// // \' single quote we use to give string in qutoe
console.log("A JavaScript \'string stores")


// // \"" double quote use given a string double quote
console.log("A JavaScript \"string stores")

// // \r carrage return uses
console.log("A JavaScript \r string stores")

let a = "navgurukul";
let b = "Dharamsala";
console.log(a.slice(3,7))
console.log(a.slice(3))
console.log(a.replace("nav" , "shiv"))
console.log(a.concat(" boy campus in ", b, " Thank you"))


let n = "      shivam       ";
let m = "      kumar        ";
console.log(n.trim())
console.log(m)

let ind = "Dharamshala";
console.log(ind[5])


// type conversion
let s = "10";
let r = Number(s);
console.log(typeof r , r)

// upper case method
let upper = "shivam";
console.log(upper.toUpperCase())

// lower case method
let lower = "SHIVAM";
console.log(lower.toLowerCase())

// Template literals in use back-ticks 
let x = 10
let y = 20
console.log(`The sum of ${x} and ${y} is = ${x+y}`)





